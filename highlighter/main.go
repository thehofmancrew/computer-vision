package main

import (
	"flag"
	"fmt"
	"github.com/owulveryck/onnx-go"
	"log"
	"os"

	"github.com/owulveryck/onnx-go/backend/x/gorgonnx"
)

func main() {
	model := flag.String("model", "onnx/model.onnx", "path to the model file")
	input := flag.String("input", "images/normal_test/cast_ok_0_265.jpeg", "path to the input file")
	output := flag.String("output", "images/out.jpeg", "path to the output file")
	// Create a backend receiver
	backend := gorgonnx.NewGraph()
	// Create a model and set the execution backend
	m := onnx.NewModel(backend)

	// read the onnx model
	b, err := os.ReadFile(*model)
	if err != nil {
		log.Fatal(err)
	}
	// Decode it into the model
	if err := m.UnmarshalBinary(b); err != nil {
		log.Fatal(err)
	}
	// Set the first input, the number depends of the model
	// TODO
	b, err = os.ReadFile(*input)
	if err != nil {
		log.Fatal(err)
	}
	inputT, err := onnx.NewTensor(b)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(inputT.Data())
	m.SetInput(0, inputT)

	if err := backend.Run(); err != nil {
		log.Fatal(err)
	}
	//b, err = os.ReadFile(*output)
	//if err != nil {
	//	log.Fatal(err)
	//}
	//outputT, err := onnx.NewTensor(b)
	//if err != nil {
	//	log.Fatal(err)
	//}
	computedOutputT, err := m.GetOutputTensors()
	if err != nil {
		log.Fatal(err)
	}
	// write the first output to stdout
	if err := os.WriteFile(*output, []byte(fmt.Sprint(computedOutputT[0])), os.ModePerm); err != nil {
		log.Fatal(err)
	}
	fmt.Println("output shape: ", computedOutputT[0].Shape())
	//assert.InDeltaSlice(&testingT{}, outputT.Data(), computedOutputT[0].Data(), 5e-3, "the two tensors should be equal.")
	fmt.Println("output data: ", computedOutputT[0].Data())
}

type testingT struct{}

func (t *testingT) Errorf(format string, args ...interface{}) {
	log.Fatalf(format, args...)
}
