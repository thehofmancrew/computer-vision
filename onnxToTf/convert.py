import onnx
from onnx_tf.backend import prepare

model = onnx.load('model.onnx')

prepare(model).export_graph('model')
