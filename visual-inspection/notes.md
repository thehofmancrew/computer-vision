* segregate few images to get more images and more random kinds of defects
* supervised learning for anomaly detection is probably not possible (no labeling)
* Generative Adversarial Network (**GAN**) architecture are the
  most prominent method in this class of problems, especially
  in computer vision. Unlike traditional classification methods,
  the GAN-trained discriminator learns to detect false from
  real in an unsupervised fashion, which leads GAN to an
  attractive unsupervised machine learning technique for
  anomaly detection [33]
* So the idea is that the **GAN** creates an _ok_ image from a _defected_. And the deviation is amplified.
* so the basic idea is that the **Generator** builds artifical images (that have not yet been in reality)
* **GAN** are used to maintain lab image equality [Batch Equalization](https://youtu.be/qvBhp0e-Kuc?t=681)
* the **Discriminator** estimates if those image are real and both get optimized on each other
* it feels like the steady state of the duality might be arbitrary and depends on initialization weights
* **Generator** gets 1d input noise to generate 2D images
* **GAN** optimization switches loss-output signs because each step is either generator or discriminator optimization
* (none,200,200,3) are 200px*200px RGB images
* Only for visualization not Decision! colormap jet, available on opencv: check gocv.ApplyColorMap()
  ![img.png](img.png)

> Even with these limitations, in a side-by-side test with production line supervisors evaluating the same images, the
> system was able to indicate all errors pointed out by supervisors. The main advantage of this system over the human
> operator is the evaluation time, **requiring only 3 seconds per image**. In addition, the system is low cost and
> simple to
> set, while not requiring parameter adjustments.