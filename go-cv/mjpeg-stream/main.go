// What it does:
//
// This example opens a video capture device, then streams MJPEG from it.
// Once running point your browser to the hostname/port you passed in the
// command line (for example http://localhost:8080) and you should see
// the live video stream.
//
// How to run:
//
// mjpeg-streamer [camera ID] [host:port]
//
//		go get -u github.com/hybridgroup/mjpeg
// 		go run ./cmd/mjpeg-streamer/main.go 1 0.0.0.0:8080
//

package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/hybridgroup/mjpeg"
	"gocv.io/x/gocv"
)

var (
	deviceID int
)

func main() {
	if len(os.Args) < 3 {
		fmt.Println("How to run:\n\tmjpeg-streamer [camera ID] [host:port]")
		return
	}

	// parse args
	deviceID := os.Args[1]
	host := os.Args[2]

	// open webcam
	webcam, err := gocv.OpenVideoCapture(deviceID)
	if err != nil {
		fmt.Printf("Error opening capture device: %v\n", deviceID)
		return
	}
	defer webcam.Close()

	// create the mjpeg stream
	stream := mjpeg.NewStream()

	// start capturing
	errChan := make(chan error, 1)
	go func() {
		errChan <- mjpegCapture(stream, webcam)
	}()

	fmt.Println("Capturing. Point your browser to " + host)

	// start http server
	http.Handle("/", stream)

	server := &http.Server{
		Addr:         host,
		ReadTimeout:  60 * time.Second,
		WriteTimeout: 60 * time.Second,
	}
	go func() {
		errChan <- server.ListenAndServe()
	}()

	if err := <-errChan; err != nil {
		log.Fatal(err)
	}
}

func mjpegCapture(stream *mjpeg.Stream, webcam *gocv.VideoCapture) error {
	img := gocv.NewMat()
	defer img.Close()

	for {
		if ok := webcam.Read(&img); !ok {
			return fmt.Errorf("Device closed: %v\n", deviceID)
		}
		if img.Empty() {
			continue
		}

		buf, err := gocv.IMEncode(".jpg", img)
		if err != nil {
			return err
		}
		stream.UpdateJPEG(buf.GetBytes())
		buf.Close()
	}
}
